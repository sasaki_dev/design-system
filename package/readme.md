# Sasaki Strategies Design System

###This package contains:
  - img
    - Strategies logos as pngs (4 options)
  - styles
    - core.scss (variables and mixins only)
    - fonts.scss (font imports only)
    - styles.scss (applied styles)

###Find the documentation at [sasakistrategies.com/design-system](http://sasakistrategies.com/design-system/)

###To update this package (admin):
  - Run "npm version [patch, minor, major]"
  - Run "npm publish"

###To install/update in your project:
  - Install `npm install --save @strategies/design-system` or `yarn add @strategies/design-system`
  - Update `npm update @strategies/design-system`
  
###To reference in your project:
  - Webpack (e.g. React): `@import '~/@strategies/design-system/styles/styles';`
  - SCSS (without webpack): `@import 'node_modules/@strategies/design-system/styles/styles';`
